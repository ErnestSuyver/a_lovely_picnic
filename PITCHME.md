# Brill Bibliographies


![home_hero](https://gitlab.com/ErnestSuyver/a_lovely_picnic/blob/master/home_hero5.png)

---

@snap[north]
## Imagine
@snapend

@snap[west span-100]
@ul[spaced text-black]

- a website for bibliographies that meets user requirements
- automatic styling of citations and references
- support for reference managers and other tools

@ulend
@snapend

---

@snap[north]
## Business case
@snapend

@snap[west span-55]
@ul[spaced text-black]

- maintain revenu
- create revenu
- save costs
- return on investment

@ulend
@snapend

---

@snap[north]
## Datafication
@snapend

@snap[west span-55]
@ul[spaced text-black]

- from content
- via **data**
- to _products_ and _services_

@ulend
@snapend

---

@snap[north span-100]
## Production and consumption
@snapend

@snap[west span-55]
@ul[spaced text-black]

- CMS
- author tool
- PDF
- site
- widget

@ulend
@snapend

---

@snap[north]
## Radical simplicity
@snapend

@snap[west span-55]
@ul[spaced text-black]

- open source
- installed base
- ecosystem

@ulend
@snapend

---

@snap[north]
## Zotero
@snapend

@snap[west span-55]
@ul[spaced text-black]

- client
- word processor plug-in
- browser extension
- API
- database

@ulend
@snapend

---

@snap[north]
## DIY
@snapend

@snap[west span-100]
@ul[spaced text-black]

- in-house development team
- supported by external developers
- supervised by stakeholders

@ulend
@snapend

